/**
 * Attack library
 * 
 * Contains plans for attacking an enemy
 */
 
 
 +!defend(Agent) : enemy(Enemy)
	<- .print("Agent: ", Agent, " defending from Agent", Enemy).