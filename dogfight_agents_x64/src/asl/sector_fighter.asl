/*
* Agent, that uses team tactics and maneuvers
* @author Patrik Cigas
*/

/* Initial beliefs and rules */

// libs includes
{ include("libraries/lib_attack.asl")}
{ include("libraries/lib_missile_firing.asl")}
{ include("libraries/lib_team.asl")}
{ include("libraries/lib_sector.asl")}

// firing facts for agent
firing_distance(1000).
firing_minimum_distance(500).
firing_aspect(15).
firing_aot(30).

// Maneuvering parameters in meters
yoyo_height(450).
spiral_height(1000).
pursuit_offset(200).
defensive_offset(50).

// self identification
myself("attacker").

// determine if target is alive
alive(Agent) :- team(Agent, _).

current_tactics("none").


/* Initial goals */

// new cycle starts with new tick
// (agent believes, that there is new tick)
+tick(_): i_am_leader & current_tactics("none") <-
	.print("Planning next tactics");
	!plan_maneuver.

+tick(_): current_tactics("positioning") <-
	!inform_leader;
	?wait(X);
	if (X > 0) {
		-+wait(X-1);
		?target(Enemy);
		?yoyo_height(Height);
		high_yoyo(Enemy, Height);
	} else {
		-+current_tactics("attack");
		-wait(_);
		!attack;
	}.
	
+tick(_): current_tactics("attack") <-
	!inform_leader;
	!check_for_target; 
	!attack(Agent).
	
+tick(_): current_tactics("spreading_out") <-
	!inform_leader;
	?wait(X);
	if (X > 0) {
		-+wait(X-1);
		?spreading(Direction, Enemy, Width);
		spread_out(Direction, Width, Enemy);
	} else {
		-+current_tactics("attack");
		-spreading(_,_,_);
		-wait(_);
		!attack;
	}.
	
+tick(_): current_tactics("sandwiching") | current_tactics("splitting") <-
	!inform_leader;
	?wait(X);
	if (X > 0) {
		-+wait(X-1);
	} else {
		-+current_tactics("attack");
		-wait(_);
		!attack;
	}.
	
+tick(_) <-
	!inform_leader.


/* Check if there is a target */

// if target is dead, we have to switch
//+!check_for_target : target(Agent) & dead(Agent)
//	<- .print("Agent is dead: ", Agent);
//	-target(Agent);
//	!pick_target.

// if there is target alive, don't do anything
+!check_for_target : target(Agent) & alive(Agent) 
	//.
	<- .print("I already have target: ", Agent).

// otherwise agent has to pick target
+!check_for_target 
	<-
	.print("Spreading out");
	!pick_target.

/* Pick the target -> pick based on importance */
// agent can only pick target if there is an enemy
// so that agent selects first unified target
// @ TODO: smarter agent picking (separation, distance, aspect), ATTACK X DEFEND
// @ TODO: tell the target, that he is already selected (occupied)
//		or tell to other friendly allies
+!pick_target : enemy(Agent) & alive(Agent)
	<- .print("Attacker picking new target"); -+target(Agent).

+!pick_target
	<- .print("Cannot pick target, everyone is dead, lets celebrate :))").

+!inform_leader: i_am_leader <-
	?enemies(Enemies).
	
+!inform_leader <-	
	?enemies(Enemies);
	?team_leader(Leader);
	.my_name(Me);
	if (.length(Enemies) == 1) {
		.nth(0, Enemies, Enemy);
		?distance(Enemy, X);
		.send(Leader, untell, distance(Me, Enemy, _));
		.send(Leader, tell, distance(Me, Enemy, X));
	} else { 
		for ( .member(Enemy, Enemies)) {
			?distance(Enemy, X);
			.send(Leader, untell, distance(Me, Enemy, _));
			.send(Leader, tell, distance(Me, Enemy, X));
		}
	}.


