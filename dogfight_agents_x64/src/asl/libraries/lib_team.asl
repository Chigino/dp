/**
 * Plans and rules related to team relations
 */

// gets the name of an enemy agent
enemy(Agent) :- 
	team(Agent, OtherTeam) & 
	team(MyTeam) & 
	MyTeam \== OtherTeam.

// gets the name of allied agent
friend(Agent) :- 
	team(Agent, A) & 
	team(A).
	
wingman(Agent) :-
	.my_name(Me) &
	team(Team) &
	team(Agent, Team) &
	Me \== Agent.

// returns the list of enemy agents
enemies(Enemies) :- 
	.findall(X, enemy(X), Enemies).

// returns the list of allied agents
friends(Friends) :- 
	.findall(X, friend(X), Friends).

// determine if 
alone(true) :- 
	friends(Friends) & 
	.empty(Friends).	// friend list empty
alone(false) :- true.

team_leader(TeamLeader) :- friends(Friends) &
						.my_name(AgentName) &
						.concat(Friends, [AgentName], AllFriendlies) &
						.min(AllFriendlies, TeamLeader).