/**
 * Sector communtication library
 * 
 * Contains plans for agent communication and planning sector maneuvers
 * @author Patrik Cigas
 */
 
small_bracket_width(400).
big_bracket_width(800).
 
i_am_leader:- .my_name(X) & team_leader(Y) & X == Y.
 
 
+!plan_maneuver: enemies(Enemies) & .length(Enemies, X) & X <2 & .nth(0, Enemies, Enemy) & target_behind(Enemy) <-
	.my_name(Me);
	?wingman(Wingman);
	?distance(Enemy, Distance);
	?distance(Wingman, Enemy, FriendDistance);
	?defensive_offset(Offset);
	if (math.abs(Distance - FriendDistance) > Offset) {
	.print("Using sandwich");
		!sandwich(Enemy, Wingman, MyDistance, WingmansDistance);
	} else {
		.print("Using half split");
		!half_split(Wingman);
	}
	.print("Defending from behind").
	
+!plan_maneuver: enemies(Enemies) & .length(Enemies, X) & X <2 & .nth(0, Enemies, Enemy) & target_in_front(Enemy) & target_very_high_aot(Enemy) 
<-
	-+current_tactics("small_bracket");
	!small_bracket(Enemy);
	?wingman(Wingman);
	.send(Wingman, achieve, set_tactics("small_bracket"));
	.send(Wingman, achieve, small_bracket(Enemy));
	.print("Using small bracket").
 
+!plan_maneuver: enemies(Enemies) & .length(Enemies, X) & X >=2 & .nth(0, Enemies, Enemy) & target_in_front(Enemy) & target_very_high_aot(Enemy) 
<-
	-+current_tactics("big_bracket");
	!big_bracket(Enemy);
	?wingman(Wingman);
	.nth(1, Enemies, Enemy2)
	.send(Wingman, achieve, set_tactics("big_bracket"));
	.send(Wingman, achieve, big_bracket(Enemy2));
	.print("Using big bracket").
	
+!plan_maneuver: enemies(Enemies) & .length(Enemies, X) & X <2 & .nth(0, Enemies, Enemy) & target_in_front(Enemy)
<-	
	-+target(Enemy);
	-+current_tactics("attack");
	!attack(Enemy);
	?wingman(Wingman);
	.send(Wingman, achieve, gain_position(Enemy, 75));
	.print("attacking").
	
+!plan_maneuver <- 
	-+current_tactics("attack");
	?wingman(Wingman);
	.send(Wingman, achieve, set_tactics("attack"));
	.send(Wingman, achieve, pick_target);
	.send(Wingman, achieve, attack);
	!pick_target;
	!attack(Agent).
	
+!defensive_split: friend_position(left) <-
	-+current_tactics("splitting");
	-+wait(30);
	turn(left).
	
+!defensive_split: friend_position(right) <-
	-+current_tactics("splitting");
	-+wait(30);
	turn(right).
	
+!half_split(Wingman): friend_position(right) <-
	-+current_tactics("splitting");
	-+wait(20);
	.send(Wingman, achieve, carryOn);
	turn(right).
	
+!half_split(Wingman): friend_position(left) <-
	-+current_tactics("splitting");
	-+wait(20);
	.send(Wingman, achieve, carryOn);
	turn(left).

+!carryOn <-
	-+current_tactics("splitting");
	-+wait(20);
	continue_forward.

+!sandwich(Enemy, Wingman, MyDistance, WingmansDistance): MyDistance < WingmansDistance & friend_position(left) <-
	-+target(Enemy);
	.send(Wingman, achieve, set_target(Enemy));
	.send(Wingman, achieve, sandwich(left));
	!sandwich(left).
	
+!sandwich(Enemy, Wingman, MyDistance, WingmansDistance): MyDistance < WingmansDistance & friend_position(right) <-
	-+target(Enemy);
	.send(Wingman, achieve, set_target(Enemy));
	.send(Wingman, achieve, sandwich(right));
	!sandwich(right).
	
+!sandwich(Enemy, Wingman, MyDistance, WingmansDistance): MyDistance > WingmansDistance & friend_position(right) <-
	-+target(Enemy);
	.send(Wingman, achieve, set_target(Enemy));
	.send(Wingman, achieve, sandwich(left));
	!sandwich(left).
	
+!sandwich(Enemy, Wingman, MyDistance, WingmansDistance): MyDistance > WingmansDistance & friend_position(left) <-
	-+target(Enemy);
	.send(Wingman, achieve, set_target(Enemy));
	.send(Wingman, achieve, sandwich(right));
	!sandwich(right).
	
+!sandwich(Direction) <-
	-+wait(30);
	-+current_tactics("sandwiching");
	turn(Direction).
	

+!small_bracket(Enemy): friend_position(left) <-
	-+current_tactics("spreading_out");
	?small_bracket_width(X);
	-+spreading(right, Enemy, X);
	-+wait(50).
	
+!small_bracket(Enemy): friend_position(right) <-
	-+current_tactics("spreading_out");
	?small_bracket_width(X);
	-+spreading(left, Enemy, X);
	-+wait(50).
	
+!big_bracket(Enemy): friend_position(left) <-
	?big_bracket_width(X);
	-+current_tactics("spreading_out");
	-+target(Enemy);
	-+spreading(right, Enemy, X);
	-+wait(50).
	
+!big_bracket(Enemy): friend_position(right) <-
	?big_bracket_width(X);
	-+current_tactics("spreading_out");
	-+target(Enemy);
	-+spreading(left, Enemy, X);
	-+wait(50).
	
+!set_tactics(Tac) <-
	-+current_tactics(Tac).
	
+!set_target(Enemy) <-
	-+target(Enemy).

+!gain_position(Enemy, Duration) <-
	-+target(Enemy);
	-+current_tactics("positioning");
	.print("positioning");
	-+wait(Duration).
