/**
 * Attack library
 * 
 * Contains plans for attacking an enemy
 */
 
 
 // attack the enemy with maneuver and shoot him down
+!attack(Target) : enemy(Target) & target(Target)
<- //.print("Attacking enemy: ", Target); 
!proceed_maneuver(Target);
!fire(Target).

+!attack(Target)
<- .print("No enemy left"). 

+!attack: target(Enemy) <-
	!attack(Enemy).
	
+!attack <-
	!pick_target;
	?target(Enemy);
	!attack(Enemy).
 
// proceed with frontside based maneuvers
+!proceed_maneuver(Target)
	: target_in_front(Target)
	<- //.print("Target ", Target, " is in front of me");
	!forward_maneuver(Target).	
	
// proceed with backside based maneuvers
+!proceed_maneuver(Target)
	: target_behind(Target)
	<- //.print("Target ", Target, " is behind me");
	!backward_maneuver(Target).
	
/*
 * Forward side maneuvering
 */

// low aot => pursuit curve
// determine type of curve by separation

// high distance target
+!forward_maneuver(Target)
	: target_low_aot(Target) &
	target_high_distance(Target) &
	pursuit_offset(PursOffset)
	<- //.print("Huge separation => Lead pursuit on enemy in front: ", Target);
	lead_pursuit(Target, PursOffset).	// LEAD PURSUIT

// medium distance target
+!forward_maneuver(Target)
	: target_low_aot(Target) &
	target_medium_distance(Target)
	<- //.print("Medium separation => Pure pursuit on enemy in front: ", Target);
	pure_pursuit(Target).	// PURE PURSUIT

// low distance target
+!forward_maneuver(Target)
	: target_low_aot(Target) &
	target_low_distance(Target) &
	yoyo_height(YoyoHeight)
	<- //.print("Low separation => Lag pursuit on enemy in front with height gain: ", Target);
	high_yoyo(Target, YoyoHeight).	// HIGH YOYO


// medium aot => get behind target using high yoyo
// or lower aot using lag pursuit

// low distance
+!forward_maneuver(Target)
	: target_medium_aot(Target)	&
	target_low_distance(Target) &
	pursuit_offset(PursOffset)
	<- //.print("Low distance enemy in front: ", Target);
	lag_pursuit(Target, PursOffset).		// LAG PURSUIT

// medium distance
+!forward_maneuver(Target)
	: target_medium_aot(Target)	&
	target_medium_distance(Target) &
	yoyo_height(YoyoHeight)
	<- //.print("High YoYo at enemy in front: ", Target);
	high_yoyo(Target, YoyoHeight).	// HIGH YOYO

// high distance
+!forward_maneuver(Target)
	: target_medium_aot(Target)	&
	target_high_distance(Target) &
	pursuit_offset(PursOffset)
	<- //.print("Lead pursuit to lower separation to enemy in front: ", Target);
	lead_pursuit(Target, PursOffset).		// LEAD PURSUIT

// high aot
// determine between head-to head and pursuit

+!forward_maneuver(Target)
	: target_high_aot(Target) &
	pursuit_offset(PursOffset)	// -> LAG PURSUIT (turn to enemy)
	<- //.print(" High aot -> pursuit: ", Target);
	lag_pursuit(Target, PursOffset).

// very high aot => practically head-to-head attack
// gain offset and keep it, then in right moment start turning to enemy
+!forward_maneuver(Target)
	: target_very_high_aot(Target)	// -> LEAD TURN TODO !!!!!!!!
	<- //.print(" HEAD-TO-HEAD, Gain offset with lead turn to front enemy: ", Target);
	gain_offset(Target).



/*
 * Backward side maneuvering
 */


// minimum aot
+!backward_maneuver(Target)
	: target_minimum_aot(Target) &
	spiral_height(SpiralHeight)
	<- .print("Backward medium aot to: ", Target, "Spiral required");
	defensive_spiral(Target, SpiralHeight).


// prepare to defend -> try to attack with High YoYo
// low aot
+!backward_maneuver(Target)
	: target_low_aot(Target) &
	spiral_height(SpiralHeight)
	<- .print("Backward low aot to: ", Target);
	high_yoyo(Target, SpiralHeight).



// really high yoyo could do the trick
// medium aot
+!backward_maneuver(Target)
	: target_medium_aot(Target) &
	yoyo_height(YoyoHeight)
	<- .print("Backward medium aot to: ", Target, "YoYo required");
	high_yoyo(Target, YoyoHeight).
	


// lead turn with y gain
// high aot
+!backward_maneuver(Target)
	: target_high_aot(Target) &
	yoyo_height(YoyoHeight)
	<- .print("Backward high aot to: ", Target);
	high_yoyo(Target, YoyoHeight).
	
	
// very high aot
+!backward_maneuver(Target)
	: target_very_high_aot(Target) &
	yoyo_height(YoyoHeight)
	<- .print("Backward very high aot to: ", Target);
	high_yoyo(Target, YoyoHeight).
			
+!backward_maneuver(Target).


/* 
 * determine enemy position - in front / behind (Based on Aspect)
 */
target_in_front(Target) :-
	aspect(Target, Aspect) &
	Aspect <= 90.
	
target_behind(Target) :-
	aspect(Target, Aspect) &
	Aspect > 90.

/* 
 * determine enemy direction - (Based on AoT)
 */

// AOT < 30�
target_minimum_aot(Target) :-
	angle_off_tail(Target, Aot) &
	Aot < 30.
 
 
// AOT <= 60�
// target heading almost same direction
// IN FRONT -> get firing position
// BEHIND   -> defensive spiral
target_low_aot(Target) :-
	angle_off_tail(Target, Aot) &
	Aot <= 60.


// AOT > 60� && AOT <= 90�
// target different direction, YoYo required
// IN FRONT -> High YoYo - gain height with lead pursuit
// BEHIND   -> Lead turn - gain height with lag pursuit
target_medium_aot(Target) :-
	angle_off_tail(Target, Aot) &
	Aot > 60 &
	Aot <= 90.


// AOT > 90� & AOT < 150
// targets opposite direction
// IN FRONT -> head-to-head .. gain offset
// BEHIND   -> behind, gain height
target_high_aot(Target) :-
	angle_off_tail(Target, Aot) &
	Aot > 90 &
	Aot <= 150.

// AOT > 150 => HEAD-TO-HEAD attack
target_very_high_aot(Target) :-
	angle_off_tail(Target, Aot) &
	Aot > 150.


/* 
 * determine separation to enemy
 */

// distance lower than firing distance
 target_low_distance(Target) :-
 	distance(Target, Distance)  &
 	firing_minimum_distance(MinDistance) &
 	Distance < MinDistance.
 
 // fine distance => firing distance ok
 target_medium_distance(Target) :-
 	distance(Target, Distance)  &
 	firing_minimum_distance(MinDistance) &
 	firing_distance(MaxDistance)&
 	Distance >= MinDistance &
 	Distance <= MaxDistance.
 	
 // huge distance - cannot fire, has to get to firing range
 target_high_distance(Target) :-
 	distance(Target, Distance)  &
 	firing_distance(MaxDistance)&
 	Distance > MaxDistance.
 	 