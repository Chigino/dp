// Agent firing in project dogfight_agents_x64

firing_distance_satisfied(Target) :- 
	distance(Target, Distance) & 
	firing_distance(FiringDistance) & 
	Distance < FiringDistance.
	
firing_aspect_satisfied(Target) :- 
	aspect(Target, Aspect) & 
	firing_aspect(FiringAspect) & 
	Aspect < FiringAspect & 
	Aspect > -FiringAspect.

firing_aot_satisfied(Target) :- 
	angle_off_tail(Target, Aot) & 
	firing_aot(FiringAot) & 
	Aot < FiringAot & 
	Aot > -FiringAot.
	
firing_parameters_satisfied(Target) :- 
	firing_distance_satisfied(Target)&
	firing_aspect_satisfied(Target) & 
	firing_aot_satisfied(Target) & 
	fire_enabled(true).							// add fire enabled percept
	
+!fire(Target): 
	enemy(Target) & 
	alive(Target) &
	firing_parameters_satisfied(Target) & 
	distance(Target, Distance) &
	angle_off_tail(Target, Aot)  & 
	aspect(Target, Aspect)
	<-  .print("Firing! dist: ", Distance, "m, ", " asp: ", Aspect, "�, ", " aot: ", Aot, "�" );
		fire(Target);
		-target(Target).
		
+!fire(Target).		// plain body so that plan is valid without context
