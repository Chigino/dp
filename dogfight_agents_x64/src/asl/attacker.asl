// Agent attacker in project dogfight_agents_x64

/* Initial beliefs and rules */

// libs includes
{ include("libraries/lib_attack.asl")}
{ include("libraries/lib_missile_firing.asl")}
{ include("libraries/lib_team.asl")}

// firing facts for agent
firing_distance(1000).
firing_minimum_distance(500).
firing_aspect(15).
firing_aot(30).

// Maneuvering parameters in meters
yoyo_height(300).
spiral_height(1000).
pursuit_offset(200).

// self identification
myself("attacker").

// determine if target is alive
alive(Agent) :- team(Agent, _).


/* Initial goals */

// new cycle starts with new tick
// (agent believes, that there is new tick)
+tick(_) 
	<-!check_for_target; 
	!attack(Agent).


/* Check if there is a target */

// if target is dead, we have to switch
//+!check_for_target : target(Agent) & dead(Agent)
//	<- .print("Agent is dead: ", Agent);
//	-target(Agent);
//	!pick_target.

// if there is target alive, don't do anything
+!check_for_target : target(Agent) & alive(Agent) 
	//.
	<- .print("I already have target: ", Agent).

// otherwise agent has to pick target
+!check_for_target 
	<- !pick_target.

/* Pick the target -> pick based on importance */
// agent can only pick target if there is an enemy
// so that agent selects first unified target
// @ TODO: smarter agent picking (separation, distance, aspect), ATTACK X DEFEND
// @ TODO: tell the target, that he is already selected (occupied)
//		or tell to other friendly allies
+!pick_target : enemy(Agent) & alive(Agent)
	<- .print("Attacker picking new target"); -+target(Agent).

+!pick_target
	<- .print("Cannot pick target, everyone is dead, lets celebrate :))").


//+!attack_or_defend

