package dogfight_agents.math;

import dogfight_agents.network.Protocol;

public class Physics {
	
	
    public static float degToRad(float deg){
        return deg * (180 / FastMath.PI);

    }
    
    public static float radToDeg(float rad){
        
        return rad * (180 / FastMath.PI);
    }
    
    /*
     * Computes turn rate of Aircraft based on speed in meters per second
     */
    public static float computeTurnRateAero(float speed){
        
    	speed *= Protocol.MACH_TO_MPS;
    	
        return speed * speed / Protocol.G_TAN_83;
        
    }
    
    /*
     * Compute seek2 => seek orthogonal to velocity if current checkpoint is
     * behind aeroplane and needs to turn
     */    
    public static Vector3f computeSeekOrthogonal(Vector3f pos, Vector3f vel, Vector3f check){
        
        // copy position and velocity vector
        Vector3f position = new Vector3f(pos);
        Vector3f velocity = new Vector3f(vel);
        
        // compute separation
        float separation = position.distance(check);
        
        // compute aspect
        Vector3f seek = new Vector3f(check.subtract(position));
        float asprads  = velocity.normalizeLocal().angleBetween(seek.normalizeLocal());
        
        
        float x = (float) Math.abs(Math.cos(asprads) * separation);
        
        Vector3f point = new Vector3f(position.addLocal(velocity.multLocal(x * (-1))));
        Vector3f orthoseek = new Vector3f(check.subtract(point));
        
        return orthoseek.normalizeLocal();
    }
    
}
