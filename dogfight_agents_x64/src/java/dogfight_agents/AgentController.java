package dogfight_agents;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import dogfight_agents.AircraftAgent;
import dogfight_agents.DogfightEnv;
import dogfight_agents.AgentController;
import dogfight_agents.math.Vector3f;
import dogfight_agents.math.FastMath;
import dogfight_agents.math.Physics;
import dogfight_agents.network.Protocol;
import dogfight_agents.network.SocketClient;
import dogfight_agents.network.json.JSONObject;

/**
 * Class DogfightModel - manages the agent simulation on client side
 * All the aeroplanes are held in this class.
 * All the network communication goes through this class.
 * 
 * @author Radek Burda
 *
 */
public final class AgentController {

	// parent environment
	private DogfightEnv environment;

	// agent aircrafts
	private HashMap<String, AircraftAgent> aircrafts;
	private int numOfAgents;

	// missiles
	private HashMap<String, Missile> missiles;

	private int redCount;
	private int blueCount;
	private int initialSeparation;
	private int initialDirection;

	// client connection to server
	private SocketClient client;

	/**
	 * Constructor
	 * @param blueTeam - blue team planes count
	 * @param redTeam - read team planes count
	 * @param incomingEnvironment - parent environment
	 */
	AgentController(int blueTeam, int redTeam, int initSep, int initDir, DogfightEnv incomingEnvironment) {

		// aeroplanes and missiles data structure init
		aircrafts = new HashMap<>();
		missiles = new HashMap<>();
		
		// parent (agents environment)
		environment = incomingEnvironment;

		// network adress and port
		client = new SocketClient("localhost", 9990);
		
		// create aeroplanes and add to hashmap structure
		int team = 1;
		for (int i = 1; i <= blueTeam + redTeam; i++) {

			// set agent name
			String aircraftName = Protocol.agentPrefix + i;
			if (i > blueTeam) {
				team = 2;
			}

			// create aero model and add to hashmap
			AircraftAgent aircraft = new AircraftAgent(new Vector3f(),
					new Vector3f(), aircraftName, team, environment);
			aircrafts.put(aircraftName, aircraft);
			
			
			// set missile name
			String missileName = aircraftName + Protocol.missileSuffix;
			
			// create missiles and add to hashmap
			Missile missile = new Missile(new Vector3f(),
					new Vector3f(), missileName, team, environment);
			missiles.put(missileName, missile);
			
		}
		blueCount = blueTeam;
		redCount = redTeam;
		initialSeparation = initSep;
		initialDirection = initDir;
		numOfAgents = blueCount + redCount;
		
	}

	
	/****************************************
	 *********** NETWORK RELATED ************
	 ****************************************/
	
	/**
	 * Connects to server and executes initial communication
	 * @param timeout - connection timeout
	 * @return
	 */
	int initNetwork(int timeout) {
		
		int retval = 1;
		
		try {
			// try to init connection and send some data
			client.connect();
			client.setSoTimeout(timeout);

			// exchange init message with server
			client.initServer(blueCount, redCount, initialSeparation, initialDirection);
			retval = 0;
			
		} catch (UnknownHostException e) {
			System.err.println("Host unknown. Cannot establish connection");
			
		} catch (IOException e) {
			System.err.println("Cannot establish connection. Server may not be up." + e.getMessage());
		}
		
		// (retval == 0) => OK
		return retval;
	}

	/**
	 * Function gets all the relevant data from server
	 */
	void updateFromServer() {
		
		String str;
		ArrayList<String> messages;
		
		try {

			// send update request
			client.sendMessage(Protocol.DATA_REQ, "Send me some data");

			// get the meta response
			str = client.getMessage(Protocol.DATA_ACC);
			
			JSONObject obj = new JSONObject(str);
			int messageCount = obj.getInt("msgcnt");
			
			// recieve of all the update messages 
			//	-> positions, velocities, missiles, deaths
			messages = new ArrayList<>();
			for(int i=0; i<messageCount; i++){
				str = client.getMessage(Protocol.DATA_UPDATE);
				messages.add(str);
			}
			
			// update local model from new data
			processMessages(messages);
			
			
		} catch (IOException e) {
			System.err.println("Error occurred when communicating with server for update: " + e.getMessage());
		}		
	}
	
	/**
	 * Gets list of JSON messages from server,
	 * then processes all the update messages => updates agent environment
	 * @param messages
	 */
	private void processMessages(ArrayList<String> messages) {

		JSONObject obj;
		String agname; 
		AircraftAgent aero;
		Missile miss;	
		
		
		// list all the messages
		for(String m : messages){
			
			// get the target of the message
			obj = new JSONObject(m);
			agname = obj.getString("name");
			
			// determine if its missile or agent
			if(agname.contains("missile")){
				
				miss = missiles.get(agname);
				
				// only if missile is active we care
				boolean active = obj.getBoolean("active");
				if(active){
					// updates the missile location and velocity
					updateAircraftObject(obj, miss);

				}
			
			// okay its agent
			} else {
			
				// get corresponding aircraft
				aero = aircrafts.get(agname);
				
				// see if is alive
				boolean alive = obj.getBoolean("alive");
				if(!alive){
					// now we have to kill agent
					aero.setAlive(false);
					System.out.println("Agent " + agname + " is dead.");
					return;
				}
				// updates aircraft location and velocity
				updateAircraftObject(obj, aero);
				
				// fire enabled
				aero.setFireEnabled(obj.getBoolean("fire_en"));	
			}
		}		
	}

	private void updateAircraftObject(JSONObject obj, AircraftAgent object) {
		// location
		object.getLocation().setX((float) obj.getDouble("locx"));
		object.getLocation().setY((float) obj.getDouble("locy"));
		object.getLocation().setZ((float) obj.getDouble("locz"));

		// velocity
		object.getVelocity().setX((float) obj.getDouble("velx"));
		object.getVelocity().setY((float) obj.getDouble("vely"));
		object.getVelocity().setZ((float) obj.getDouble("velz"));

		// speed
		object.setSpeed((float) obj.getDouble("speed"));

		// target
		object.setServerEnemy(obj.getString("target"));
	}


	/**
	 * Send the action requests to server
	 */
	void updateToServer(){
			
		try {

			// prepare data structures
			ArrayList<String> messages = new ArrayList<>();
			
			// iterate over all agents and prepare messages for server
	        Collection<AircraftAgent> aircraftCollection = getAircrafts().values();
	    	for (AircraftAgent aero : aircraftCollection) {

				// Prepare the client ACTION message if aero is alive
	    		if(aero.isAlive()){
		    		String str = SocketClient.createAircraftActionMessage(aero);
		    		messages.add(str);
	    		}
    		}

	    	// DO THE BULK NETWORK COMMUNICATION
			// send update request
			client.sendMessage(Protocol.ACTION_REQ, "ACTION META INFO ...");

			// send all messages to srv
	    	for(String m : messages){
	    		client.sendMessage(Protocol.ACTION_UPDATE, m);
	    	}
				
		} catch (IOException e) {
			System.err.println("Error occurred when communicating with server for update: " + e.getMessage());
		}		

		
	}
	
	/**
	 * Updates model state variables
	 */
	void updateLocalModel(){
		
        Collection<AircraftAgent> aircraftCollection = getAircrafts().values();
    	for (AircraftAgent aero : aircraftCollection) {
    		aero.setFiringFlag(false);
    	}
		
	}
	
	
	/****************************************
	 ***** ACTIONS RELATED COMPUTATIONS *****
	 ****************************************/

	/**
	 * Returns distance in meters of two airplanes
	 * @param agName
	 * @param otherAgentName
	 * @return
	 */
	int getSeparationFromTo(String agName, String otherAgentName) {
		
		Vector3f p1 = getAircraftByAgName(agName).getLocation();
		Vector3f p2 = getAircraftByAgName(otherAgentName).getLocation();
		
		return (int) p1.distance(p2);
	}

	
	/**
	 * Returns AOT of 2 aeroplanes in degrees
	 * @param agName
	 * @param otherAgentName
	 * @return
	 */
	int getAngleOffTailFromTo(String agName, String otherAgentName) {
		// get velocity vector of both planes
		Vector3f v1 = new Vector3f(getAircraftByAgName(agName).getVelocity());
		Vector3f v2 = new Vector3f(getAircraftByAgName(otherAgentName).getVelocity());
		
		// angle in rads
		float angrads = v1.normalizeLocal().angleBetween(v2.normalizeLocal());
		
		// angle in degrees
		return Math.round(Physics.radToDeg(angrads));
	}
	
	
	/**
	 * Returns the Aspect in degrees to other plane
	 * @param agName
	 * @param otherAgentName
	 * @return
	 */
	int getAspectFromTo(String agName, String otherAgentName) {

		AircraftAgent myAero = getAircraftByAgName(agName);
		AircraftAgent otherAero = getAircraftByAgName(otherAgentName);
		
		
		// angle between my velocity vector and vector directing to other agent
		Vector3f v1 = getAircraftByAgName(agName).getVelocity().normalize();
		Vector3f v2 = new Vector3f(otherAero.getLocation().subtract(myAero.getLocation()));
		v2.normalizeLocal();
		
		float asprads = v1.angleBetween(v2);
		return Math.round(Physics.radToDeg(asprads));
	}

	
	/**
	 * Returns offset from agName to other Agent in meters
	 * @param agName
	 * @param otherAgentName
	 * @return
	 */
	int getOffsetFromTo(String agName, String otherAgentName) {
		
		float asp = getAspectFromTo(agName, otherAgentName);
		float sinAsp = (float) Math.sin(Math.toRadians(asp));

		float sep = getSeparationFromTo(agName, otherAgentName);

		
		return (int) (sinAsp * sep);
	}
	
	
	/****************************************
	 *********** GETTERS SETTERS ************
	 ****************************************/
	
	public HashMap<String, AircraftAgent> getAircrafts() {
		return aircrafts;
	}

	public AircraftAgent getAircraftByAgName(String agentName) {

		return aircrafts.get(agentName);		
	}

	public int getNbOfAgs() {
		return numOfAgents;
	}
	
	public Missile getMissileByName(String missName) {

		return missiles.get(missName);		
	}
}
