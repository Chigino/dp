package dogfight_agents;

import dogfight_agents.math.Vector3f;

class Missile extends AircraftAgent {

	Missile(Vector3f initialLoc, Vector3f initialVel, String agName, int teamNumber, DogfightEnv env) {
		super(initialLoc, initialVel, agName, teamNumber, env);
	}

}
