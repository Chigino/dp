package dogfight_agents;

import dogfight_agents.math.Vector3f;
import jason.asSyntax.*;
import jason.environment.*;

import java.util.*;
import java.util.logging.*;

import dogfight_agents.math.Physics;
import dogfight_agents.network.Protocol;

/**
 * Class represents Jason environment for agents.
 * Uses java environment and methods
 * 		addPercept -> to add Agent beliefs
 * 		executeAction -> to execute agent internal actions
 * Class AgentController keeps the network connection to server
 * and all the aeroplane data structures
 * 
 * @author Radek Burda
 */
public class DogfightEnv extends Environment {

	// dogfight model controller
	private AgentController control;

	// state variables
	private int tickNumber = 0;
	private int redTeamCount, blueTeamCount;

	// logger
	private Logger logger = Logger.getLogger("testenv.mas2j." + DogfightEnv.class.getName());

	/**
	 * Dogfigt Environment constructor
	 */
	public DogfightEnv() {
	}
	
	
	/** Called before the MAS execution with the args informed in .mas2j */
	@Override
	public void init(String[] args) {

		// init superclass - Jason environment class
		super.init(args);

		// connection to server timeout (ms)
		int TIMEOUT = 500;
		// for checking server response
		int retval;
		
		// parse teamfight relevant arguments
		blueTeamCount = Integer.parseInt(args[1]);
		redTeamCount= Integer.parseInt(args[2]);
		int initialSep = Integer.parseInt(args[3]);
		int initialDir = Integer.parseInt(args[4]);
		
		// create agent controller with the aeroplanes
		control = new AgentController(blueTeamCount, redTeamCount, initialSep, initialDir, this);
		
		// init network connection between client and server model
		if((retval = control.initNetwork(TIMEOUT)) != 0){
			System.err.println("Failed to initialize network, exiting now...");
			System.exit(1);
		}

		// Create jason environment
		new Thread() {
			// run the thread
			public void run() {
				try {
					// MAIN LOOP
					while (isRunning()) {
						// sleep for synchronization step
						Thread.sleep(Protocol.SYNCHRO_STEP);
						
						// get new positions, velocities and actions (missiles)
						// from server
						control.updateFromServer();
						
						// update Agents perceptions
						updateAllAgsPercept();

						// inform environment changed (Jason env native)
						informAgsEnvironmentChanged();
						
						// send actions to server
						control.updateToServer();
						
						// kill poor dead agents
						killDeadAgents();

						tickNumber ++;
						logger.info("tick(" + tickNumber + ")");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			};
		}.start();
	}

	@Override
	public boolean executeAction(String agName, Structure action) {
		
		// find corresponding Aero
		AircraftAgent aircraft = control.getAircraftByAgName(agName);
		// get action functor
		String functor = action.getFunctor();

		logger.info("Agent" + agName + " executing: " + action);

		// REPORT STUFF
		if (functor.equals("report")) {
			aircraft.report();

		// LEAD PURSUIT
		} else if (functor.equals("lead_pursuit")) {
			String enemy = action.getTerm(0).toString();
			int pursuitOffs = Integer.parseInt(action.getTerm(1).toString());
			aircraft.leadPursuit(enemy, pursuitOffs);

		// PURE PURSUIT
		} else if (functor.equals("pure_pursuit")) {
			String enemy = action.getTerm(0).toString();
			aircraft.purePursuit(enemy);
		
		// LAG PURSUIT
		} else if (functor.equals("lag_pursuit")) {
			String enemy = action.getTerm(0).toString();
			int pursuitOffs = Integer.parseInt(action.getTerm(1).toString());
			aircraft.lagPursuit(enemy, pursuitOffs);

		// HIGH YOYO
		} else if (functor.equals("high_yoyo")) {
			String enemy = action.getTerm(0).toString();
			int yoyoheight = Integer.parseInt(action.getTerm(1).toString());
			aircraft.highYoYo(enemy, yoyoheight);

		// GAIN OFFSET
		} else if (functor.equals("gain_offset")) {
			String enemy = action.getTerm(0).toString();
			int turnrate = (int) Physics.computeTurnRateAero(aircraft.getSpeed());
			aircraft.gainOffset(enemy, turnrate * 2);

		// DEFENSIVE SPIRAL
		} else if (functor.equals("defensiveSpiral")) {
			String enemy = action.getTerm(0).toString();
			int spiralheight = Integer.parseInt(action.getTerm(1).toString());
			aircraft.defensiveSpiral(enemy, spiralheight);

			
		// SHOOT 'EM DOWN
		} else if (functor.equals("fire")) {
			String target = action.getTerm(0).toString();
			aircraft.fire(target);

		// CHANGING SPEED
		} else if (functor.equals("desired_speed")) {
			String newSpeed = action.getTerm(0).toString();

		} else if (functor.equals("turn")) {
			String side = action.getTerm(0).toString();
			aircraft.turn(side);

		} else if (functor.equals("spread_out")) {
			String side = action.getTerm(0).toString();
			int spreadWidth = Integer.parseInt(action.getTerm(1).toString());
			String enemy = action.getTerm(2).toString();
			aircraft.spreadOut(side, spreadWidth, enemy);

		} else if (functor.equals(("continue_forward"))) {
			aircraft.continueForward();
			
		} else {
			logger.info("executing: " + action + ", but not implemented!");
		}
		return true;
	}

	/**
	 * Function takes care about updating agents perceptions,
	 * thus updating agents beliefs.
	 * First it removes all old perceptions and then adds new ones
	 * about self and then about other agents.
	 * 
	 * @param agentName
	 * @param agNumber
	 */
	private void updateSingleAgPercept(String agentName) {

		// clear perceptions of an agent
		clearPercepts(agentName);

		// initial belief, starts whol agent process
        addPercept(agentName, Literal.parseLiteral("tick(" + tickNumber + ")"));

		// get java representation of aero and and percepts to corresponding
		AircraftAgent aircraft = control.getAircraftByAgName(agentName);

		// am I even alive?
		addPercept(agentName, Literal.parseLiteral("alive(" + aircraft.getAlive() + ")"));

		// add info if agent can fire missile
		addPercept(agentName, Literal.parseLiteral("fire_enabled(" + aircraft.getFireEnabled() + ")"));

		// add team information for current agent
		addPercept(aircraft.getAgentName(), Literal.parseLiteral("team(" + aircraft.getTeam() + ")"));

		// iterate over all other agents and add relation based beliefs
        Collection<AircraftAgent> aircraftCollection = control.getAircrafts().values();
    	for (AircraftAgent foreignAircraft : aircraftCollection) {

    		// get the name
			String otherAgentName = foreignAircraft.getAgentName();
    		
    		
    		// if agent is alive different from current agent
    		if ( foreignAircraft.isAlive() && (!otherAgentName.equals(aircraft.getAgentName()))  ) {		
    			
    			// Team
    			addPercept(agentName, Literal.parseLiteral("team(" + otherAgentName + ", " + foreignAircraft.getTeam() + ")"));

    			// they are same team and know each other position
    			if (foreignAircraft.getTeam() == aircraft.getTeam()) {
    				addPercept(agentName, Literal.parseLiteral("friend_position(" + getPositionToAircraft(aircraft, foreignAircraft) + ")"));
				}

    			// Separation
    			int distance = control.getSeparationFromTo(agentName, otherAgentName); 
    			addPercept(agentName, Literal.parseLiteral("distance(" + otherAgentName + ", " + distance + ")"));
    			
    			// AOT
    			int angleOffTail = control.getAngleOffTailFromTo(agentName, otherAgentName);
    			addPercept(agentName, Literal.parseLiteral("angle_off_tail(" + otherAgentName + ", " + angleOffTail + ")"));

    			// Aspect
    			int aspect = control.getAspectFromTo(agentName, otherAgentName);
    			addPercept(agentName, Literal.parseLiteral("aspect(" + otherAgentName + ", "+ aspect + ")"));

    			// Aspect from other aero's point of view
    			int reverseAspect = control.getAspectFromTo(otherAgentName, agentName); 
    			addPercept(agentName, Literal.parseLiteral("reverse_aspect(" + otherAgentName + ", "+ reverseAspect + ")"));
    			
    			// Offset
    			int offset = control.getOffsetFromTo(agentName, otherAgentName);
    			addPercept(agentName, Literal.parseLiteral("offset(" + otherAgentName + ", "+ offset + ")"));    			
    		
    			// explicitly tell that the enemy is dead
    		} else if ((!otherAgentName.equals(aircraft.getAgentName()))) {
    			addPercept(agentName, Literal.parseLiteral("dead(" + otherAgentName + ")"));

    		}
    	}
	}

	private String getPositionToAircraft(AircraftAgent aircraft, AircraftAgent foreignAircraft) {
		Vector3f aircraftVector = new Vector3f(
				aircraft.getLocation().x - foreignAircraft.getLocation().x,
				aircraft.getLocation().y - foreignAircraft.getLocation().y,
				aircraft.getLocation().z - foreignAircraft.getLocation().z);
		return ((aircraftVector.x + aircraftVector.y + aircraftVector.z) > 0) ? "right" : "left";
	}

	/**
	 * Updates all the agents and their perceptions
	 * Calls updateSingleAgPercept i loop over all agents
	 */
	private void updateAllAgsPercept() {
		for (int i = 1; i <= control.getNbOfAgs(); i++) {
			updateSingleAgPercept(Protocol.agentPrefix + i);
		}
	}

	
	/**
	 * Informs Jason environment about dead agents
	 */
	private void killDeadAgents() {
		Collection<String> aircraftCollection = getEnvironmentInfraTier().getRuntimeServices().getAgentsNames();
		for (String agentName : aircraftCollection) {
			AircraftAgent aircraft = control.getAircraftByAgName(agentName);
			// kill agent if he is not alive
			if (!aircraft.getAlive()) {
				getEnvironmentInfraTier().getRuntimeServices().killAgent(agentName, agentName);
			}
		}
	}
	
	
	/****************************************
	 *********** GETTERS SETTERS ************
	 ****************************************/
	
	public AgentController getAgentController(){
		return control;
	}
	
}
