package dogfight_agents;

import java.io.Serializable;

import dogfight_agents.math.Physics;
import dogfight_agents.math.Vector3f;
import dogfight_agents.network.Protocol;

public class AircraftAgent implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// Environment
	DogfightEnv environment;
	
	// Agent name and team
	private String agentName;
	private int team;

	// State variables
	private boolean isAlive;
	private Vector3f velocity;
	private Vector3f location;
	private boolean firing;
	private boolean fireEnabled;

	// current and desired speed
	private float currSpeed;
	private float desiredSpeed;

	// enemy and checkpoint to go to
	private String enemy;
	private String serverEnemy;	// enemy on server (maybe useful)
	private Vector3f checkpoint;
	
	/**
	 * AircraftAgent constructor
	 * 
	 * @param initialLoc - initial location
	 * @param initialVel - initial velocity
	 * @param agName - agent name
	 * @param teamNumber - team -> 1 X 2
	 * @param env - agent environment
	 */
	AircraftAgent(Vector3f initialLoc, Vector3f initialVel, String agName, int teamNumber, DogfightEnv env) {

		// Init location and velocity
		location = new Vector3f(initialLoc);
		velocity = new Vector3f(initialVel);
		checkpoint = new Vector3f();
		
		// environment
		environment = env;
		
		// State variables
		agentName = agName;
		team = teamNumber;
		isAlive = true;
			
		currSpeed = 1;
		desiredSpeed = 1;
		enemy = "NOONE";
		firing = false;
		fireEnabled = false;
		
	}

	
	/****************************************
	 ************ AGENT ACTIONS *************
	 ****************************************/

	void report() {
		System.out.println("Agent " + agentName + " report on action duty.");
		
		
	}
	
	/**
	 * Fires at enemy target
	 * @param enemyAero
	 */
	void fire(String enemyAero) {
		enemy = enemyAero;
		if (fireEnabled)
			firing = true;
	}


	/**
	 * Lead pursuit curve -> directs (pursuit offset) m in front of enemy
	 * Is used for rapid distance lowering
	 * @param enemy
	 */
	void leadPursuit(String enemy, int pursOffset) {

		
		// get enemy by name
		AircraftAgent enemyAero = environment.getAgentController().getAircraftByAgName(enemy);
		
		// get enemy position and velocity
		Vector3f enemyPos = new Vector3f(enemyAero.getLocation());
		Vector3f enemyVel = new Vector3f(enemyAero.getVelocity());
		float enemySpd = enemyAero.getSpeed();
		
		// compute new location based on SYNCHRO_STEP
		checkpoint =  compensate(enemyPos, enemyVel, enemySpd);
		
		float simOffset = (float) pursOffset / 100;
		
		checkpoint.addLocal(enemyVel.mult(simOffset));
		
		// save enemy for server info
		this.enemy = enemy;

	}
	
	/**
	 * Pure Pursuit curve -> directly follow enemy, used for
	 * distance lowering and minimizung AOT
	 * @param enemy
	 */
	void purePursuit(String enemy) {
		
		// get the aero from collection
		AircraftAgent enemyAero = environment.getAgentController().getAircraftByAgName(enemy);
		
		// get enemy position and velocity
		Vector3f enemyPos = new Vector3f(enemyAero.getLocation());
		Vector3f enemyVel = new Vector3f(enemyAero.getVelocity());
		float enemySpd = enemyAero.getSpeed();
		
		// compute new location based on SYNCHRO_STEP
		checkpoint =  compensate(enemyPos, enemyVel, enemySpd);
				
		// save enemy for server info
		this.enemy = enemy;
		
	}

	/**
	 * Lag pursuit curve -> directs (pursuit offset) m behind the enemy
	 * Is used for rapid AOT lowering
	 * @param enemy
	 */
	void lagPursuit(String enemy, int pursOffset) {
		
		// get enemy by name
		AircraftAgent enemyAero = environment.getAgentController().getAircraftByAgName(enemy);
		
		// get enemy position and velocity
		Vector3f enemyPos = new Vector3f(enemyAero.getLocation());
		Vector3f enemyVel = new Vector3f(enemyAero.getVelocity());
		float enemySpd = enemyAero.getSpeed();
		
		// compute new location based on SYNCHRO_STEP
		checkpoint =  compensate(enemyPos, enemyVel, enemySpd);
		checkpoint.subtractLocal(enemyVel.mult(2));
		
		// save enemy for server info
		this.enemy = enemy;

	}
	
	/**
	 * High yoyo - based on lagpursuit, just gains height advantage
	 * @param enemy
	 * @param yoyoHeight
	 */
	void highYoYo(String enemy, int yoyoHeight){
		
		lagPursuit(enemy, 200);
		checkpoint.y += yoyoHeight;
		
		// save enemy for server info
		this.enemy = enemy;

	}

	/**
	 * Gain offset - generates offset and then proceeds lead turn
	 * @param enemy
	 * @param offset
	 */
	void gainOffset(String enemy, int offset){
		
		
		// get enemy by name
		AircraftAgent enemyAero = environment.getAgentController().getAircraftByAgName(enemy);
		
		// get enemy position and velocity
		Vector3f enemyPos = new Vector3f(enemyAero.getLocation());
		Vector3f enemyVel = new Vector3f(enemyAero.getVelocity());


		Vector3f abstractCheckpoint = new Vector3f(enemyPos);
		
		Vector3f abstractSeek = enemyPos.subtract(this.getLocation());
		abstractCheckpoint.addLocal(abstractSeek);
		
		Vector3f seekRev = Physics.computeSeekOrthogonal(enemyPos, enemyVel, abstractCheckpoint);
		// get the right direction
		seekRev.negateLocal().multLocal(offset);
		
		// move the point to offset
		checkpoint = new Vector3f(enemyPos);	
		checkpoint.addLocal(seekRev);
		
		// save enemy for server info
		this.enemy = enemy;

	}
	
	void defensiveSpiral(String enemy, int spiralHeight){
		
		purePursuit(enemy);
		checkpoint.y -= spiralHeight;
		
		// save enemy for server info
		this.enemy = enemy;
	}

	/**
	 * Method for turning the airplane to side
	 * @author Patrik Cigas
	 * @param side
	 */
	void turn(String side) {
		checkpoint = compensate(location, velocity, currSpeed);
		turnToDirection(checkpoint, side, 1200);
	}

	/**
	 * Method for spreading out sectors formation
	 * @author Patrik Cigas
	 * @param side
	 * @param width
	 * @param enemy
	 */
	void spreadOut(String side, int width, String enemy) {
		// get enemy by name
		AircraftAgent enemyAero = environment.getAgentController().getAircraftByAgName(enemy);

		checkpoint = compensate(enemyAero.getLocation(), enemyAero.getVelocity(), enemyAero.getSpeed());
		checkpoint.x = velocity.x > 0 ? 500 : -500;
		turnToDirection(checkpoint, side, width);
	}

	/**
	 * Method for continuing in current flight direction
	 * @author Patrik Cigas
	 */
	void continueForward() {
		checkpoint = compensate(location, velocity, currSpeed);
		checkpoint.addLocal(velocity.mult(1.5f));

		checkpoint.x += velocity.x > 0 ?  800 : -800;
	}
	
	/**
	 * Fixes the position lag caused by network communication
	 * @param pos - current position Vector3f
	 * @param vel - current velocity Vector3f
	 * @param spd - current speed float
	 * @return compensated position Vector3f
	 */
	private Vector3f compensate(Vector3f pos, Vector3f vel, float spd){
		
		Vector3f compensated = new Vector3f(pos);
		
		// get the amount of updates per second
		float comPerSec = 1000 / Protocol.SYNCHRO_STEP;	
		
		// because there is a lag, compensate 1/comPerSec of flight path each update
		float compenMultip = 1 / comPerSec;
		
		// *3.3 because velocity magnitude is 100 already .. so we get m*s-1
		float velociMultip = compenMultip * spd * 3.3f;	
				
		// compensate position lag by velocity multiplier
		compensated.addLocal(vel.mult((velociMultip)));
		
		return compensated;
	}

	/**
	 * Method for setting turn direction and offset
	 * @author Patrik Cigas
	 * @param check
	 * @param direction
	 * @param offset
	 */
	private void turnToDirection(Vector3f check, String direction, int offset) {
		if (direction.equals("left")) {
			check.z += velocity.x > 0 ?  -offset : offset;
		}
		else if (direction.equals("right")) {
			check.z += velocity.x > 0 ?  offset : -offset;
		}
	}

	
	
	/****************************************
	 *********** GETTERS SETTERS ************
	 ****************************************/
	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public int getTeam() {
		return team;
	}

	public void setTeam(int team) {
		this.team = team;
	}

	public Vector3f getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector3f velocity) {
		this.velocity = velocity;
	}

	public Vector3f getLocation() {
		return location;
	}

	public void setLocation(Vector3f location) {
		this.location = location;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public boolean getAlive() {
		return isAlive;
	}

	public String getEnemy() {
		return enemy;
	}
	
	public float getSpeed(){
		return currSpeed;
	}

	public Vector3f getCheckPoint() {
		return checkpoint;
	}


	public boolean getFiringFlag() {
		return firing;
	}
	
	public void setFiringFlag(boolean ff) {
		firing = ff;
	}


	public void setSpeed(float spd) {
		currSpeed = spd;
	}

	public void setFireEnabled(boolean fe){
		fireEnabled = fe;
	}

	public boolean getFireEnabled(){
		return fireEnabled;
	}
	
	public void setServerEnemy(String servEnemy){
		this.serverEnemy = servEnemy;
	}
	
	public String getServerEnemy(){
		return this.serverEnemy;
	}
}
