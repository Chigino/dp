package dogfight_agents.network;

public class Protocol {

    public static final String agentPrefix = "aero_";
    public static final String missileSuffix = "_missile";

    /** Client-Server Protocol constants **/

    public static final int END = -1;           // End message
    public static final int START_INIT_REQ = 0; // Client to Server initial message
    public static final int START_INIT_ACC = 1; // Server to client initial confirm
    public static final int DATA_REQ = 2;       // Cliend needs data from Server
    public static final int DATA_ACC = 3;       // Server confirms and sends META message
    public static final int DATA_UPDATE = 4;    // Server sending update to client
    public static final int ACTION_REQ = 5;     // Client to server agent action required
    public static final int ACTION_ACC = 6; // Server to client action confirm
    public static final int ACTION_UPDATE = 7; // Server to client action confirm


    public static final float BANK_AERO = 83.5f;    // aeroplane bank = 83.5 degrees
    public static final float BANK_MISS = 88;       // missile bank = 88 degrees
    public static final float G_CONST = 9.81f;      // gravitational acceleration = 9.81 m.s-2

    public static final float TAN_835 = 8.77689f;   // tan(83.5�)
    public static final float TAN_83 = 8.1443f;   // tan(83.5�)
    public static final float TAN_86 = 14.3f;   // tan(83.5�)
    public static final float TAN_87 = 19.0811f;   // tan(83.5�)
    public static final float TAN_88 = 28.6363f;   // tan(83.5�)

    public static final float G_TAN_835 = 86.10126f;   // g * tan(83.5�)
    public static final float G_TAN_83 = 79.896f;   // g * tan(83.5�)
    public static final float G_TAN_86 = 140.2895f;   // g * tan(87�)
    public static final float G_TAN_87 = 187.186f;   // g * tan(87�)
    public static final float G_TAN_88 = 280.9216f;   // g * tan(88�)

    public static final float MACH_TO_MPS = 330;
    public static final float MACH_TO_KMH = 1188;
    public static final float MPS_TO_KMH = 3.6f;
    public static final float KMH_TO_MPS = 0.27777f;
    public static final float METERS_TO_SIM_SCALE = 0.01f;
    public static final int SIM_SCALE_TO_METERS = 100;


    public static final int SYNCHRO_STEP = 150;		// synchronization step in miliseconds


    public static final int ANY_MESSAGE = 1024; // Just a message


}