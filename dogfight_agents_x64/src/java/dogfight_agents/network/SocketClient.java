package dogfight_agents.network;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import dogfight_agents.AircraftAgent;
import dogfight_agents.math.Vector3f;


/**
 * Socket client class handles all the communication with server
 * @author Radek
 *
 */
public class SocketClient {

	// host, port, socket
	private String hostname;
	private int port;
	Socket socketClient;
	
	// data streams for communication
	DataInputStream dIn;	
	DataOutputStream dOut;
	
	/**
	 * Constructor
	 * @param hostname
	 * @param port
	 */
	public SocketClient(String hostname, int port) {
		this.hostname = hostname;
		this.port = port;
	}
	
	/**
	 * Connects to Server and initializes input dIn and output dOut streams 
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public void connect() throws UnknownHostException, IOException {
		
		System.out.println("Attempting to connect to " + hostname + ":" + port);
		
		// create socket and set TO
		socketClient = new Socket(hostname, port);
		socketClient.setSoTimeout(5000);	// 5 seconds

		// init in and out streams
		dIn = new DataInputStream(socketClient.getInputStream());
		dOut = new DataOutputStream(socketClient.getOutputStream());

		System.out.println("Connection Established");
	}

	
	private String createInitMsg(int blueCount, int redCount, int initSep, int initDir){
		// {"blue": X, "red": Y}
		return "{ \"blue\": " + blueCount + ","
				+ " \"red\": " + redCount + ","
				+ " \"separation\": " + initSep + ","
				+ " \"direction\": " + initDir + 
				" }";
	}
    
	/**
     * Creates client plane JSON update string for server
     * @param aero
     * @return JSON message string
     */
    public static String createAircraftActionMessage(AircraftAgent aero){
        
        Vector3f check = aero.getCheckPoint();
        
        String aeroUpdate = "{ \"name\": " + aero.getAgentName()
        		+ ", \"enemy\": " +  aero.getEnemy()
                + ", \"checkx\": " +  check.x
                + ", \"checky\": " +  check.y
                + ", \"checkz\": " +  check.z
                + ", \"fire\": " +  aero.getFiringFlag()
                + " }";
        
        // after we shot, we cannot anymore
        aero.setFiringFlag(false);
        
        return  aeroUpdate;
    }
	
	/**
	 * Does the initial communication with server (handshake)
	 * @throws IOException
	 */
	public void initServer(int blueCount, int redCount, int initSep, int initDir) throws IOException {
		
		String initMsg = createInitMsg(blueCount, redCount, initSep, initDir);
		System.out.println("INITMSMG: " + initMsg);	
		
		// Send first message
		dOut.writeByte(Protocol.START_INIT_REQ);
		dOut.writeUTF(initMsg);
		dOut.flush(); // Send off the data
		
		
		// wait for server confirmation
		byte messageType = dIn.readByte();
		if(messageType == Protocol.START_INIT_ACC){
			System.out.println("START_INIT_ACC: " + dIn.readUTF());
		} else {
			System.out.println("UNKNOWN MESSAGE TYPE --> Initial handshake failed..");
			System.exit(1);
		}
	}
	
	/**
	 * Sends message to server
	 * @param firsByte - message type (DATA_REQ, ACTION_REQ)
	 * @param message - message itself (JSON structure)
	 * @throws IOException
	 */
	public void sendMessage(int firsByte, String message) throws IOException{
		
		// Send first message
		dOut.writeByte(firsByte);
		dOut.writeUTF(message);
		dOut.flush(); // Send off the data
	}
	
	/**
	 * Receives message from server, returns JSON string,
	 * also in the first byte of communication there is a message type stored
	 * @param requiredMessType - message type (DATA_UPDATE)
	 * @return
	 * @throws IOException
	 */
	public String getMessage(int requiredMessType) throws IOException{
				
		// string to return
		String str = "";
		
		// Read the server response
		byte messageType = dIn.readByte();
		str = str.concat(dIn.readUTF());
		
		// see if got correct message type
		if((messageType != requiredMessType) && (requiredMessType != Protocol.ANY_MESSAGE)){
			System.err.println("DIFFERENT MESSAGE TYPE: " + messageType
					+ ", REQUIRED: " + requiredMessType);

			throw new IOException("Wrong message type.");

		}
		
		// if received successfully, return
		return str;
	}
	
	/**
	 * Sets client socket timeout
	 * @param t - timeout in miliseconds
	 * @throws SocketException
	 */
	public void setSoTimeout(int t) throws SocketException{
		socketClient.setSoTimeout(t);
	}
		
}